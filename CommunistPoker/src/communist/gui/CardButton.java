package communist.gui;

import java.awt.Dimension;

import javax.swing.*;

import communist.poker.Card;

public class CardButton extends JButton {
	public Card card;
	private int icon_width;
	private int icon_height;
	
	public CardButton() {
		ImageIcon cover = new ImageIcon("./Cards/Card Cover.png");
		this.setIcon(cover);
		this.setPreferredSize(new Dimension(cover.getIconWidth(), cover.getIconHeight()));
		icon_width = cover.getIconWidth();
		icon_height = cover.getIconHeight();
	}
	public CardButton(Card c) {
		update_card(c);
	}

	public void update_card(Card c) {
		card = c;
		this.setIcon(card.get_icon());
		this.setPreferredSize(new Dimension(c.get_icon().getIconWidth(), c.get_icon().getIconHeight()));
		icon_width = c.get_icon().getIconWidth();
		icon_height = c.get_icon().getIconHeight();
	}
	public int get_width() {
		return icon_width;
	}
	public int get_height() {
		return icon_height;
	}
}
