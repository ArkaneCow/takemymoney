package communist.gui;

import javax.swing.*;

import communist.poker.Card;
import communist.poker.Deck;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.*;
import java.util.ArrayList;

public class PokerGUI extends JFrame implements ActionListener {

	private static final int COST = 1;
	private static final int ROYAL_FLUSH = 250;
	private static final int STRAIGHT_FLUSH = 50;
	private static final int FOUR_OF_A_KIND = 25;
	private static final int FULL_HOUSE = 6;
	private static final int FLUSH = 5;
	private static final int STRAIGHT = 4;
	private static final int THREE_OF_A_KIND = 3;
	private static final int TWO_PAIR = 2;
	private static final int TWO_OF_A_KIND = 1;

	private Table table;
	private Deck cards;
	private CardButton[] hand;
	private HoldLabel[] hand_hold;
	private JButton draw_button = new JButton("Draw");
	private boolean game_start = false;
	private ArrayList<Integer> holds = new ArrayList<Integer>();
	private int game_state = 0;
	private JTextField credits_field;
	private int credits = 5;

	private void new_hand() {
		hand = new CardButton[5];
		hand_hold = new HoldLabel[5];
		int divide = (int) (table.getWidth() / 5);
		for (int i = 0; i < 5; i++) {
			hand_hold[i] = new HoldLabel();
			hand[i] = new CardButton();
			hand[i].setBounds(300 + i * (divide + hand[i].get_width() + 20),
					30, hand[i].get_width(), hand[i].get_height());
			hand[i].setActionCommand(new Integer(i).toString() + "_card");
			hand[i].addActionListener(this);
			hand_hold[i].setBounds(300 + i
					* (divide + hand[i].get_width() + 20), 150,
					hand[i].get_width(), 25);
			table.add(hand[i]);
			table.add(hand_hold[i]);
		}
	}

	private void refresh_hand(boolean force) {
		System.out.println("refreshing hand force: " + force);
		for (int i = 0; i < 5; i++) {
			if (force || !holds.contains(new Integer(i))) {
				Card c = cards.draw_card();
				hand[i].update_card(c);
				hand_hold[i].setBackground(Color.GRAY);
			}
		}
		holds = new ArrayList<Integer>();
	}

	public PokerGUI(Deck cards) {
		this.setTitle("TAKE MY MONEY");
		this.setSize(new Dimension(800, 400));
		this.setResizable(false);
		table = new Table();
		table.setLayout(null);
		draw_button.setPreferredSize(new Dimension(draw_button
				.getPreferredSize().width * 2,
				draw_button.getPreferredSize().height));
		draw_button.setBounds(100, 50, draw_button.getPreferredSize().width,
				draw_button.getPreferredSize().height);
		draw_button.addActionListener(this);
		table.add(draw_button);
		this.cards = cards;
		this.new_hand();
		credits_field = new JTextField();
		credits_field.setBackground(Color.GRAY);
		credits_field.setEditable(false);
		int width = (int) hand[0].get_width() + 70;
		credits_field.setPreferredSize(draw_button.getPreferredSize());
		credits_field.setBounds(100, 230,
				credits_field.getPreferredSize().width,
				credits_field.getPreferredSize().height);
		table.add(credits_field);
		this.add(table);
		credits_field.setText("Credits: " + credits);
		draw_button.setText("Start");
		draw_button.setActionCommand("Draw");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
	}

	private void check_points() {
		System.out.println("checking points");
		ArrayList<ArrayList<Card>> sort = new ArrayList<ArrayList<Card>>(13);
		for (int i = 0; i < 13; i++) {
			sort.add(new ArrayList<Card>());
		}
		for (int i = 0; i < 5; i++) {
			sort.get(hand[i].card.get_rank()).add(hand[i].card);
		}
		int[] lengths = new int[13];
		int highest_length = 0;
		for (int l = 0; l < 13; l++) {
			lengths[l] = sort.get(l).size();
			if (lengths[l] > highest_length) {
				highest_length = lengths[l];
			}
		}
		if (highest_length == 4) {
			// four of a kind
			credits += FOUR_OF_A_KIND;
			return;
		} else if (highest_length == 3) {
			// three of a kind or full house
			boolean full_house = false;
			for (int h = 0; h < 13; h++) {
				if (lengths[h] == 2) {
					full_house = true;
					// full house
					JOptionPane.showMessageDialog(this, "Full House", "Prize",
							JOptionPane.INFORMATION_MESSAGE);
					credits += FULL_HOUSE;
					return;
				}
			}
			// no full house - three of a kind
			JOptionPane.showMessageDialog(this, "Three of a Kind", "Prize",
					JOptionPane.INFORMATION_MESSAGE);
			credits += THREE_OF_A_KIND;
			return;
		} else if (highest_length == 2) {
			int times = 0;
			for (int d = 0; d < 13; d++) {
				if (lengths[d] == 2) {
					times++;
				}
			}
			if (times == 2) {
				// double pair
				JOptionPane.showMessageDialog(this, "Double Pair", "Prize",
						JOptionPane.INFORMATION_MESSAGE);
				credits += TWO_PAIR;
				return;
			} else {
				int d_index = -1;
				for (int e = 0; e < 13; e++) {
					if (lengths[e] == 2) {
						d_index = e;
					}
				}
				if (d_index == 0 || d_index >= 10) {
					// two of a kind
					JOptionPane.showMessageDialog(this, "Two of a Kind",
							"Prize", JOptionPane.INFORMATION_MESSAGE);
					credits += TWO_OF_A_KIND;
					return;
				} else {
					// nothing
					JOptionPane.showMessageDialog(this, "Nothing", "Prize",
							JOptionPane.INFORMATION_MESSAGE);
				}
			}
		} else {
			// test for straight or flush
			// cue some inefficient and/or incorrectly implemented bubble
			// sort
			// algorithm
			Card[] hand_cards = new Card[5];
			for (int c = 0; c < 5; c++) {
				hand_cards[c] = hand[c].card;
			}
			// some arbitrary number of iterations?
			for (int n = 0; n < 10; n++) {
				for (int a = 0; a < 4; a++) {
					Card c_a = hand_cards[a];
					Card c_b = hand_cards[a + 1];
					if (c_b.get_rank() < c_a.get_rank()) {
						hand_cards[a] = c_b;
						hand_cards[a + 1] = c_a;
					}
				}
			}
			boolean straight = false;
			if (hand_cards[1].get_rank() == hand_cards[0].get_rank() + 1
					&& hand_cards[2].get_rank() == hand_cards[1].get_rank() + 1
					&& hand_cards[3].get_rank() == hand_cards[2].get_rank() + 1
					&& hand_cards[4].get_rank() == hand_cards[3].get_rank() + 1) {
				straight = true;
			}
			if (!straight) {
				// there is still hope
				
				if (hand_cards[0].get_rank() != 0
						|| hand_cards[4].get_rank() != 12) {
					// nothing
					JOptionPane.showMessageDialog(this, "Nothing", "Prize",
							JOptionPane.INFORMATION_MESSAGE);
					return;
				}
				boolean consecutive = true;
				int check_index = 0;
				while (consecutive) {
					if (hand_cards[check_index + 1].get_rank() != hand_cards[check_index]
							.get_rank() + 1) {
						consecutive = false;
					}
				}
				int sum_s = 0;
				for (int s = 0; s <= check_index; s++) {
					sum_s += hand_cards[s].get_rank();
				}
				int sum_l = 0;
				for (int l = check_index + 1; l <= 4; l++) {
					sum_l += hand_cards[l].get_rank();
				}
				double avg_s = ((double) hand_cards[0].get_rank() + (double) hand_cards[check_index]
						.get_rank()) / (double) 2;
				double avg_l = ((double) hand_cards[check_index + 1].get_rank() + (double) hand_cards[4]
						.get_rank()) / (double) 2;
				if ((avg_s * (double) (check_index - 0 + 1) == (double) sum_s)
						&& avg_l * (double) (12 - (check_index + 1) + 1) == (double) sum_l) {
					straight = true;
				} else {
					// nothing
					JOptionPane.showMessageDialog(this, "Nothing", "Prize",
							JOptionPane.INFORMATION_MESSAGE);
					return;
				}
			}
			boolean flush = false;
			if (hand[0].card.get_suit() == hand[1].card.get_suit()
					&& hand[1].card.get_suit() == hand[2].card.get_suit()
					&& hand[2].card.get_suit() == hand[3].card.get_suit()
					&& hand[3].card.get_suit() == hand[4].card.get_suit()) {
				flush = true;
			}
			if (straight && flush) {
				boolean royal_flush = false;
				if (sort.get(9).size() == 1 && sort.get(0).size() == 1) {
					// royal flush
					JOptionPane.showMessageDialog(this, "Royal Flush", "Prize",
							JOptionPane.INFORMATION_MESSAGE);
					credits += ROYAL_FLUSH;
					return;
				}
				// straight flush
				JOptionPane.showMessageDialog(this, "Straight Flush", "Prize",
						JOptionPane.INFORMATION_MESSAGE);
				credits += STRAIGHT_FLUSH;
				return;
			}
			if (straight) {
				JOptionPane.showMessageDialog(this, "Straight", "Prize",
						JOptionPane.INFORMATION_MESSAGE);
				credits += STRAIGHT;
				return;
			}
			if (flush) {
				JOptionPane.showMessageDialog(this, "Flush", "Prize",
						JOptionPane.INFORMATION_MESSAGE);
				credits += FLUSH;
				return;
			}
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals("Draw")) {
			if (game_start) {
				if (game_state == 0) {
					System.out.println("game state 0");
					refresh_hand(false);
					check_points();
					credits_field.setText("Credits: " + credits);
					draw_button.setText("Another Round?");
					draw_button.setActionCommand("Draw");
					cards.shuffle();
					game_state = 1;
				} else if (game_state == 1) {
					if (credits -1 < 0) {
						JOptionPane.showMessageDialog(this,
								"YOU ARE NOW BROKE", "BANK NOTICE",
								JOptionPane.ERROR_MESSAGE);
						// Monty Hall
					} else {
						draw_button.setText("Draw");
						draw_button.setActionCommand("Draw");
						System.out.println("game state 1");
						credits -= COST;
						credits_field.setText("Credits: " + credits);
						refresh_hand(true);
						game_state = 0;
					}
				}
			} else {
				draw_button.setText("Draw");
				draw_button.setActionCommand("Draw");
				game_start = !game_start;
				System.out.println("game started");
				credits -= COST;
				credits_field.setText("Credits: " + credits);
				refresh_hand(true);
			}
		} else if (e.getActionCommand().endsWith("_card")) {
			if (game_start) {
				int index = Integer
						.parseInt(e.getActionCommand().split("_")[0]);
				if (holds.contains(new Integer(index))) {
					hand_hold[index].setBackground(Color.GRAY);
					holds.remove(new Integer(index));
				} else {
					hand_hold[index].setBackground(Color.YELLOW);
					holds.add(new Integer(index));
				}
			}
		}
	}

}
