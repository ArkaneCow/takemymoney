package communist.gui;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;
import javax.swing.*;

public class Table extends JPanel {
	
	private Image background;
	
	public Table() {
		BufferedImage img = null;
		try {
			img = ImageIO.read(new File("./background.jpg"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		background = img;
		System.out.println("table created");
	}
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(background, 0, 0, null);
	}
}
