package communist.gui;

import java.awt.Color;

import javax.swing.*;

public class HoldLabel extends JTextField {
	public HoldLabel() {
		super(10);
		this.setBackground(Color.GRAY);
		this.setEditable(false);
	}
}
