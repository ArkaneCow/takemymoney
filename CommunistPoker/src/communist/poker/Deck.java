package communist.poker;

import communist.poker.Card;
import java.util.Random;

public class Deck {
	private Card[] cards;
	
	private static final int DECK_LENGTH = 52;
	
	private int get_index_2d(int x, int y, int width) {
		return (y*width) + x;
	}
	
	public void shuffle() {
		Random r = new Random();
		for (int i = 0; i < DECK_LENGTH; i++) {
			int a_index = r.nextInt(DECK_LENGTH);
			int b_index = r.nextInt(DECK_LENGTH);
			Card a = cards[a_index];
			Card b = cards[b_index];
			cards[a_index] = b;
			cards[b_index] = a;
		}
	}
	
	public Card draw_card() {
		Card c = cards[0];
		Card[] new_deck = new Card[DECK_LENGTH];
		for (int i = 0; i < DECK_LENGTH - 1; i++) {
			new_deck[i] = this.cards[i + 1];
		}
		new_deck[DECK_LENGTH -1] = c;
		cards = new_deck;
		return c;
	}
	
	private void create_deck() {
		for (int x = 0; x < 13; x++) {
			for (int y = 0; y < 4; y++) {
				int card_index = get_index_2d(x, y, 13);
				cards[card_index] = new Card(y, x);
			}
		}
	}
	
	public Deck() {
		cards = new Card[this.DECK_LENGTH];
		this.create_deck();
		System.out.println("deck created");
		this.shuffle();
		System.out.println("deck shuffled");
	}
}
