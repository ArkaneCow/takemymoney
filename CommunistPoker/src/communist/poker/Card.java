package communist.poker;

import javax.swing.ImageIcon;

public class Card {
	private int suit;
	private int rank;

	public Card(int suit, int rank) {
		this.suit = suit;
		this.rank = rank;
	}

	public int get_suit() {
		return this.suit;
	}

	public int get_rank() {
		return this.rank;
	}

	public void set_suit(int s) {
		this.suit = s;
	}

	public void set_rank(int r) {
		this.rank = r;
	}

	public ImageIcon get_icon() {
		String r_string = "";
		String s_string = "";
		switch (this.suit) {
		case 0:
			s_string = "Heart";
			break;
		case 1:
			s_string = "Spade";
			break;
		case 2:
			s_string = "Diamond";
			break;
		case 3:
			s_string = "Club";
			break;
		default:
			break;
		}
		switch (this.rank) {
		case 0:
			r_string = "Ace";
			break;
		case 1:
			r_string = "Two";
			break;
		case 2:
			r_string = "Three";
			break;
		case 3:
			r_string = "Four";
			break;
		case 4:
			r_string = "Five";
			break;
		case 5:
			r_string = "Six";
			break;
		case 6:
			r_string = "Seven";
			break;
		case 7:
			r_string = "Eight";
			break;
		case 8:
			r_string = "Nine";
			break;
		case 9:
			r_string = "Ten";
			break;
		case 10:
			r_string = "Jack";
			break;
		case 11:
			r_string = "Queen";
			break;
		case 12:
			r_string = "King";
			break;
		default:
			break;
		}
		String path = "";
		path += "./Cards/" + s_string + "/" + r_string + " of " + s_string + ".png";
		ImageIcon icon = new ImageIcon(path);
		return icon;
	}
	public ImageIcon cover_icon() {
		ImageIcon cover_icon = new ImageIcon("./Cards/Card Cover.png");
		return cover_icon;
	}
}
